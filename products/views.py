from django.shortcuts import render
from .models import Product

def products(request):
    items = Product.objects.all()
    context = {'items':items}
    return render(request, 'product.html', context)

def product_byid(request, num):
    items = Product.objects.get(id=num)
    context = {'items':items}
    return render(request, 'productbyid.html', context)