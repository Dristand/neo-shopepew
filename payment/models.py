from django.db import models
from products.models import Product
from django.utils import timezone

# Create your models here.
class PaymentDetails(models.Model): 
    date = models.DateTimeField(default=timezone.now)

    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    firstname = models.CharField(max_length=20,default="")
    lastname = models.CharField(max_length=20,default="")
    address = models.TextField(default="")
    postalcode = models.CharField(max_length=5, default="")
    phone = models.CharField(max_length=15, default="")

    promo = models.CharField(max_length=14, default="")