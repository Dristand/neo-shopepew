from django.db import models

# Create your models here.
class Review(models.Model):

    review_content = models.CharField(max_length=300)
    reviewer_username = models.CharField(max_length=300,default="")
    no = models.CharField(max_length=10,default="")
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.review_content