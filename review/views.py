from django.shortcuts import render
from .models import Review
from .forms import ReviewForm

# Create your views here.
def reviews(request):
    return render(request, 'review/reviews.html')

def review(request):
    if request.method == "POST":
        data = json.loads(request.body)
        form = ReviewForm({'review_content' : data['review_content']})
        if form.is_valid():
            jumlah = Review.objects.all().count()
            author = User.objects.get(username=request.session['username'])
            item = form.save(commit=False)
            item.reviewer_username = author.username
            item.no = str(jumlah+1)
            item.save()
            print(Review.objects.last().review_content)
            return JsonResponse({
                'review_content' : Review.objects.last().review_content,
                'author' : Review.objects.last().reviewer_username,
                'no' : str(jumlah+1),
                'created_at' : Review.objects.last().created_at,
            })
        print(form.errors)