from django.shortcuts import render, redirect
from .models import listDiscount, promoSearch

# Create your views here.
def discount(request):
    promos = listDiscount.objects.all()

    if request.method == 'POST':
        searchForm = promoSearch(request.POST)

        if searchForm.is_valid():
            category = request.POST.get('category')
            text = request.POST.get('text')
            
            if category == "kodePromo":
                promos = listDiscount.objects.filter(kodePromo = text)
            else:
                promos = listDiscount.objects.filter(persen__gte = text)

    searchForm = promoSearch()

    return render(request, 'promo.html', {'promos' : promos, 'form' : searchForm})