from django.db import models
from django.forms import ModelForm

class Product(models.Model):
    name = models.CharField(max_length=50)
    price = models.IntegerField()
    desc = models.CharField(max_length=400, default="")
    stok = models.IntegerField(default=10)
    imglink = models.CharField(max_length=200, default="https://shopepew.herokuapp.com/static/homepage/logo%20white%201.png")

    def __str__(self):
        return self.name


class Product_Form(ModelForm):
    class Meta:
        model = Product
        fields = '__all__'