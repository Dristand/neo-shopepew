from django.test import TestCase
import datetime
from .models import Review

# Create your tests here.
def test_if_review_bar_exists(self):
    response = Client().get('/')
    content = response.content.decode('utf8')
    self.assertIn("What they said.",content)

def test_review_model(self):
    Review.objects.create(
        review_content = "Recommended!",
        reviewer_username = "wasi",
    )
    self.assertEqual(Review.objects.all().count(), 1)

def test_review_string_representation(self):
    review = Review(
        review_content = "Recommended!",
        reviewer_username = "wasi",
    )

    self.assertEqual(str(review),review.review_content)

def test_review(self):
    response = Client().post('/review',data={'review_content':'bagus banget ihhhh!!!!!'}, content_type="application/json")
    #self.assertEqual(Review.objects.all().count(),1)
    self.assertTemplateUsed('review.html')
    self.assertEqual(response.status_code, 301)
