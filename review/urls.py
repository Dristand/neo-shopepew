from django.urls import path
from .views import reviews

urlpatterns = [
    path('review/', review, name="review")
]
