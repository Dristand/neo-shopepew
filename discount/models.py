from django.db import models
from django import forms

# Create your models here.
class listDiscount(models.Model):
    expiredDate = models.DateTimeField()
    name = models.CharField(max_length = 25)
    persenDiskon = models.IntegerField()
    hargaMinimum = models.IntegerField()
    kodePromo = models.CharField(max_length = 14, default="SHOPEPEW")

    def __str__(self):
        return self.name


class promoSearch(forms.Form):

    CHOICES = [('persenDiskon','Persen Diskon'), ('kodePromo','Kode Promo')]

    category = forms.ChoiceField(choices = CHOICES, widget = forms.RadioSelect)
    text = forms.CharField(max_length = 14)