from django.forms import ModelForm
from .models import PaymentDetails

class Form(ModelForm): 
    class Meta: 
        model = PaymentDetails
        fields = '__all__'
        exclude = ('date', 'product')
