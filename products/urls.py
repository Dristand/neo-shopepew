from django.urls import path
from .views import products,product_byid

urlpatterns = [
    path('', products, name='products'),
    path('<int:num>',product_byid)
]