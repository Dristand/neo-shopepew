from django.urls import path
from .views import promo

urlpatterns = [
    path('', promo, name='promo'),
]