# Tugas Kelompok 1

Selamat Datang di Repository Kelompok PPW Fakultas Ilmu Komputer Universitas Indonesia 2019/2020

[![pipeline status](https://gitlab.com/asfiowilma/shopepew/badges/master/pipeline.svg)](https://gitlab.com/asfiowilma/shopepew/-/commits/master)

[![coverage report](https://gitlab.com/asfiowilma/shopepew/badges/master/coverage.svg)](https://gitlab.com/asfiowilma/shopepew/-/commits/master)

## Anggota Kelompok
1. Jeremy Victor Andre Napitupulu - 1906293114
2. Nur Wasi Na'wa - 1806147110
3. Valentino Herdyan Permadi - 1906350660
4. Asfiolitha Wilmarani - 1906350944

## Status Pipeline
Status pipeline sudah siap untuk deployment

## Code Coverage
Sudah ditambahkan Hello World pada landing page sebagai placeholder

## Link Heroku
https://shopepew.herokuapp.com