from django.shortcuts import render, redirect
from .models import listPromo, promoSearch

# Create your views here.
def promo(request):
    promos = listPromo.objects.all()

    if request.method == 'POST':
        searchForm = promoSearch(request.POST)

        if searchForm.is_valid():
            category = request.POST.get('category')
            text = request.POST.get('text')
            
            if category == "kodePromo":
                promos = listPromo.objects.filter(kodePromo = text)
            else:
                promos = listPromo.objects.filter(persen__gte = text)

    searchForm = promoSearch()

    return render(request, 'promo.html', {'promos' : promos, 'form' : searchForm})