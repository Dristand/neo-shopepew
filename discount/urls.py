from django.urls import path
from .views import discount

urlpatterns = [
    path('', discount, name='discount'),
]