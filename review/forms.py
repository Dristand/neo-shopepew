from django import forms
from .models import Review

attrs = {
    'class': 'form-control form-group' 
}

class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = [
            'review_content',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
                'placeholder' : 'Write your review here...'
            })
