from django.shortcuts import render

from products.models import Product
from .models import PaymentDetails
from .forms import Form

# Create your views here.
def checkout(request):
    # product = Product.objects.get(id=id)

    form = Form(request.POST or None)
    if form.is_valid():
        form.save()
        form = Form()
    
    context = {
        'form': form,
        # 'product': product
    }

    return render(request, 'payment/checkout.html', context)